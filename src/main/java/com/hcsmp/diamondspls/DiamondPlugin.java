package com.hcsmp.diamondspls;

import com.hcsmp.diamondspls.integration.HeroChatIntegration;
import com.hcsmp.diamondspls.listener.PlayerListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


public class DiamondPlugin extends JavaPlugin {

    @Getter
    private Set<UUID> approvedPlayers = new HashSet<>();

    @Override
    public void onEnable() {

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
        HeroChatIntegration.init();

    }
}
