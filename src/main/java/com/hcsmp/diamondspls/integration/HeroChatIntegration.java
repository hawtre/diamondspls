package com.hcsmp.diamondspls.integration;

import com.dthielke.ChatterManager;
import com.dthielke.Herochat;
import com.dthielke.api.Channel;
import com.dthielke.api.Chatter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class HeroChatIntegration {

    public static Plugin herochatPlugin;

    public static void init() {
        herochatPlugin = Bukkit.getPluginManager().getPlugin("Herochat");
    }

    public static boolean isGlobalChatting(Player player) {
        if (herochatPlugin != null) {
            ChatterManager chatterManager = Herochat.getChatterManager();
            Chatter sender = chatterManager.getChatter(player);
            Channel channel = sender.getActiveChannel();
            if (channel.getName().toLowerCase().equals("global")) {
                return true;
            }
        }
        return false;
    }
}