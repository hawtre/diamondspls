package com.hcsmp.diamondspls.listener;

import com.hcsmp.diamondspls.DiamondPlugin;
import com.hcsmp.diamondspls.integration.HeroChatIntegration;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

@RequiredArgsConstructor
public class PlayerListener implements Listener {

    private final DiamondPlugin plugin;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.DIAMOND_ORE && !event.getPlayer().hasPermission("diamondspls.bypass")) {
            if (!plugin.getApprovedPlayers().contains(event.getPlayer().getUniqueId())) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.GOLD + "You have to say \"diamonds pls\" before you can mine this!");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (HeroChatIntegration.isGlobalChatting(player)) {
            if (event.getMessage().toLowerCase().equals("diamonds pls")) {
                plugin.getApprovedPlayers().add(event.getPlayer().getUniqueId());
            }
        }
    }
}